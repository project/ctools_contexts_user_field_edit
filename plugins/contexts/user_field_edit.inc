<?php
/**
 * @file
 *
 * Plugin to provide a user_field_edit context
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('User field edit form'),
  'description' => t('User field edit form.'),
  'context' => 'ctools_context_user_fields_form',
  'edit form' => 'ctools_context_user_fields_form_settings_form',
  'defaults' => array('uid' => ''),
  'keyword' => 'user_edit',
  'context name' => 'user_field_edit',
  'convert list' => 'ctools_context_user_fields_convert_list',
  'convert' => 'ctools_context_user_fields_convert',
  'placeholder form' => array(
    '#type' => 'textfield',
    '#description' => t('Enter the user ID of a user for this argument:'),
  ),
);

/**
 * It's important to remember that $conf is optional here, because contexts
 * are not always created from the UI.
 */
function ctools_context_user_fields_form($empty, $data = NULL, $conf = FALSE) {
  // Determine the user category.
  $category = !empty($conf['category']) ? $conf['category'] : FALSE;
  unset($conf['category']);

  // If no category was specified, use the default 'account'.
  if (!$category) {
    $category = 'account';
  }
  // Return previously created contexts, per category.
  static $created = array();
  if (!empty($created[$category])) {
    return $created[$category];
  }

  $context = new ctools_context(array('form', 'user_edit', 'user_form', 'user_field_edit', 'user', 'entity:user'));
  // Store this context for later.
  $created[$category] = $context;
  $context->plugin = 'user_field_edit';
  if ($empty) {
    return $context;
  }

  if (!empty($conf)) {
    //The user whose fields is editing could be either a specified user whoes ID is specifed in the setting form, or current logined user.
    if (empty($data['edit_logined_user'])) {
      //The 'Edit Logined User' check boxed is not selected, then get the user id from the settings form data
      $uid = is_array($data) && isset($data['uid']) ? $data['uid'] : (is_object($data) ? $data->uid : 0);
  
      if (module_exists('translation')) {
        if ($translation = module_invoke('translation', 'user_uid', $uid, $GLOBALS['language']->language)) {
          $uid = $translation;
          $reload = TRUE;
        }
      }
  
      if (is_array($data) || !empty($reload)) {
        $account = user_load($uid);
      }
    } else {
      //The user whose fields is editing is current logined user
      global $user;
      $account = user_load($user->uid);
    }
  }

  if (!empty($account)) {
    
    $form_id = 'user_profile_form';

    $form_state = array('want form' => TRUE, 'build_info' => array('args' => array($account, $category)));

    $file = drupal_get_path('module', 'user') . '/user.pages.inc';
    require_once DRUPAL_ROOT . '/' . $file;
    // This piece of information can let other modules know that more files
    // need to be included if this form is loaded from cache:
    $form_state['build_info']['files'] = array($file);

    $form = drupal_build_form($form_id, $form_state);

    // Fill in the 'node' portion of the context
    $context->data     = $account;
    $context->title    = isset($account->name) ? $account->name : '';
    $context->argument = $account->uid;

    $context->form       = $form;
    $context->form_state = &$form_state;
    $context->form_id    = $form_id;
    $context->form_title = isset($account->name) ? $account->name : '';
    $context->restrictions['form'] = array('form');
    //Set the type property to fix the 'Undefined index' error occuring in entity_form_field.inc 
    $context->restrictions['type'] = array('user');
    return $context;
  }
}

/*
 * Field setting form function
 */
function ctools_context_user_fields_form_settings_form($form, &$form_state) {
  $conf = &$form_state['conf'];
  
  $form['edit_logined_user'] = array(
      '#title' => t('Edit the logined user'),
      '#type' => 'checkbox',
      '#default_value' => isset($conf['edit_logined_user']) ? $conf['edit_logined_user'] : true,
      '#weight' => -11,
  );

  $form['user'] = array(
    '#title' => t('Enter the name or UID of a user whose fields is editing'),
    '#type' => 'textfield',
    '#maxlength' => 512,
    '#autocomplete_path' => 'ctools/autocomplete/user',
    '#weight' => -10,
    '#states' => array('invisible' => array(':input[name="edit_logined_user"]' => array('checked' => TRUE),)
        ,),
  );

  if (!empty($conf['uid'])) {
    $info = db_query('SELECT * FROM {users} WHERE uid = :uid', array(':uid' => $conf['uid']))->fetchObject();
    if ($info) {
      $link = l(t("'%name' [user id %uid]", array('%name' => $info->name, '%uid' => $info->uid)), "user/$info->uid", array('attributes' => array('target' => '_blank', 'title' => t('Open in new window')), 'html' => TRUE));
      $form['user']['#description'] = t('Currently set to !link', array('!link' => $link));
    }
  }

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $conf['uid'],
  );

  $form['set_identifier'] = array(
    '#type' => 'checkbox',
    '#default_value' => FALSE,
    '#title' => t('Reset identifier to user name'),
    '#description' => t('If checked, the identifier will be reset to the user name of the selected user.'),
  );

  return $form;
}

/**
 * Settings form validating function
 */
function ctools_context_user_fields_form_settings_form_validate($form, &$form_state) {
  // Validate the autocomplete
  if (empty($form_state['values']['uid']) && empty($form_state['values']['user']) && empty($form_state['values']['edit_logined_user']) ) {
    form_error($form['user'], t('You must select a user.'));
    return;
  }

  if (!empty($form_state['values']['user'])) {
    $uid          = $form_state['values']['user'];
    $preg_matches = array();
    $match        = preg_match('/\[id: (\d+)\]/', $uid, $preg_matches);
    if (!$match) {
      $match = preg_match('/^id: (\d+)/', $uid, $preg_matches);
    }
  
    if ($match) {
      $uid = $preg_matches[1];
    }
    if (is_numeric($uid)) {
      $user = db_query('SELECT uid FROM {users} WHERE uid = :uid', array(':uid' => $uid))->fetchObject();
    }
    else {
      $user = db_query('SELECT uid FROM {users} WHERE LOWER(name) = LOWER(:name)', array(':name' => $uid))->fetchObject();
    }
  
    form_set_value($form['uid'], $user->uid, $form_state);
  }
}

/*
 * Setting form submit handler function
 */
function ctools_context_user_fields_form_settings_form_submit($form, &$form_state) {
  if ($form_state['values']['set_identifier'] && !empty($form_state['values']['uid'])) {
    $user = user_load($form_state['values']['uid']);
    $form_state['values']['identifier'] = $user->name;
  }

  // This will either be the value set previously or a value set by the
  // validator.
  $form_state['conf']['edit_logined_user'] = $form_state['values']['edit_logined_user'];
  //Store the edited user ID to form configuration data, if edited user is the logined user, the user ID is ''
  if (empty($form_state['values']['edit_logined_user'])) {
    $form_state['conf']['uid'] = $form_state['values']['uid'];
  } else {
    $form_state['conf']['uid'] = '';
  }
  
}

/**
 * Provide a list of ways that this context can be converted to a string.
 */
function ctools_context_user_fields_convert_list() {
  ctools_get_context('user');
  return ctools_context_user_convert_list();
}

/**
 * Convert a context into a string.
 */
function ctools_context_user_fields_convert($context, $type) {
  ctools_get_context('user');
  return ctools_context_user_convert($context, $type);
}
