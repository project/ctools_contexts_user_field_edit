<?php
/**
 * @file
 *
 * Plugin to provide a submit button for a form
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
    'title' => t('Submit Button'),
    'description' => t('Displays a submit button.'),
    'single' => TRUE,
    'content_types' => array('submit_button'),
    'render callback' => 'submit_button_content_type_render',
  //We need pass some form contexts to the render callback function, such as form ID, form tokens
    'required context' => new ctools_context_required(t('Form'), 'form'),
 // After specifying the category property, we can find out this context in the form group
    'category' => array(t('Form'), -9),
);

/**
 * Render callback function.
 *
 * @param $subtype
 * @param $conf
 * @param $args
 * @param $context
 * @return stdClass
 */
function submit_button_content_type_render($subtype, $conf, $args, $context) {
  $block = new stdClass();
  
  if (isset($context->form)) {
    //Copy all properties of the form to the block content array, including the form submit button
    foreach (array('submit', 'form_token', 'form_build_id', 'form_id', 'entity', 'actions') as $element) {
      $block->content[$element] = isset($context->form[$element]) ? $context->form[$element] : NULL;
      unset($context->form[$element]);
    }
  } else {
    list($entity_type, $property) = explode(':', $subtype, 2);
    if (empty($property)) {
      return;
    }

    if (isset($context->form)) {
      $block->content[$property] = $context->form[$property];
      unset($context->form[$property]);
    }
  }

  return $block;
}